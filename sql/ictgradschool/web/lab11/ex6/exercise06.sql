-- Answers to Exercise 6 here
DROP TABLE IF EXISTS dbtest_tableseven;

CREATE TABLE IF NOT EXISTS dbtest_tableseven (
  id INT NOT NULL,
  title VARCHAR(100) NOT NULL,
  director VARCHAR(100) NOT NULL ,
  price INT NOT NULL,
  member VARCHAR(20),

  PRIMARY KEY (id),
  FOREIGN KEY (member) REFERENCES dbtest_tablefour (name)
);
INSERT INTO dbtest_tableseven (id, title, director, price, member) VALUES
  ('1', 'The Horrors of CompSci VOL1', 'Peter', 4, 'Peter Jackson'),
  ('2', 'The Horrors of CompSci VOL2', 'Peter', 2, NULL ),
  ('3', 'The Horrors of CompSci VOL3', 'Peta', 6, 'Neil Finn'),
  ('4', 'The Horrors of CompSci VOL4', 'Peterson', 2, 'Dan Carter');