-- Answers to Exercise 8 here
DELETE FROM dbtest_tablesix WHERE lName = 'destroyer4';
ALTER TABLE dbtest_tablesix DROP COLUMN fName;
DROP TABLE dbtest_tablesix;

UPDATE dbtest_tablesix SET lName = 'BOOM' WHERE lName = 'destroyer4';

UPDATE dbtest_tablefour SET year_born = 2018 WHERE name = 'Peter Jackson';

UPDATE dbtest_tablesix SET username = 'Lord Peter' WHERE username = 'peta';



