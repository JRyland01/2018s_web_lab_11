-- Answers to Exercise 7 here
DROP TABLE IF EXISTS dbtest_tableeight;

CREATE TABLE IF NOT EXISTS dbtest_tableeight (
  articleid INT,
  id INT AUTO_INCREMENT,
  comments  VARCHAR(8000) NOT NULL,
  PRIMARY KEY (id, articleid),

  FOREIGN KEY (articleid) REFERENCES dbtest_tablefive (id)
);
INSERT INTO dbtest_tableeight (articleid,id,comments) VALUES
  (1,id,'This article sucked');