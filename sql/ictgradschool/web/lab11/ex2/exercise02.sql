-- Answers to Exercise 2 here
DROP TABLE IF EXISTS dbtest_tablethree;

CREATE TABLE IF NOT EXISTS dbtest_tablethree (
  username VARCHAR(20) NOT NULL ,
  fName VARCHAR(20) NOT NULL,
  lName VARCHAR(20) NOT NULL ,
  email VARCHAR(20) NOT NUll,
  PRIMARY KEY (username)
);
INSERT INTO dbtest_tablethree (username, fName, lName, email) VALUES
  ('dude1', 'Peter', 'destroyer1', 'peter@hotmail.com'),
  ('dude2', 'Pete', 'destroyer2', 'pete@hotmail.com'),
  ('dude3', 'Peterson', 'destroyer3', 'peterson@hotmail.com'),
  ('dude4', 'Peta', 'destroyer4', 'peta@hotmail.com');

SELECT * FROM dbtest_tablethree;