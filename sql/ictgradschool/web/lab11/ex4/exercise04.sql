-- Answers to Exercise 4 here
DROP TABLE IF EXISTS dbtest_tablefive;

CREATE TABLE IF NOT EXISTS dbtest_tablefive (
  id INT,
  title VARCHAR(50)   NOT NULL,
  text  VARCHAR(8000) NOT NULL,
  PRIMARY KEY (id)
);
INSERT INTO dbtest_tablefive (id, title, text) VALUES
  (1, '50 Shades of Gray', 'Extend the specification of the table developed for Exercise 2 above so the username is a
primary key. Do so by copying the Exercise 2 SQL into the ex5/exercise05.sql file, then
rename the table with an updated prefix before working with it further.
Experiment to see what happens when you add a row with the same username to the table
developed for the database Exercise 2. And then again, when done with the Exercise 5.' );