-- Answers to Exercise 9 here
SELECT * FROM dbtest_tablefour;

SELECT name, gender, year_born, joined FROM dbtest_tablefour;

SELECT title FROM dbtest_tablefive;

SELECT DISTINCT  director FROM dbtest_tableseven;
-- Gets no repeats DISTINCT

SELECT price FROM dbtest_tableseven WHERE price <= 2;

SELECT username FROM dbtest_tablesix ORDER BY username;
-- ASCENDING ORDER

SELECT director FROM dbtest_tableseven  WHERE director LIKE 'Pete%';
-- WHERE and LIKE

SELECT username FROM dbtest_tablesix WHERE fName LIKE 'Pete%' OR lName LIKE 'Pete%';
-- Must include LIKE OR for each check

